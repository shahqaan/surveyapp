var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var answerSchema = new Schema ({
	name:String,
	email:String,
	age:String,
	weight:String,
	height_f:String,
	height_i:String,
	gender:String,
	frame:String,
	dairy:String,
  	meat:String,
  	wheat:String,
  	vegetables:String,
  	fruits:String,
  	water:String,
  	diseases:String,
  	medicines:String,
  	sleep:String,
  	exercise:String,
  	smoke:String,
    exercise_intensity : String,
  	diseasesHistory:String
})

var answerModel = mongoose.model('answer', answerSchema);

module.exports = answerModel;