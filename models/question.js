var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var questionSchema = new Schema ({
	"question" : String,
	"type" : String,
	"values" : String
})

var questionModel = mongoose.model('question', questionSchema);

module.exports = questionModel;