angular.module('myApp').component('tableList', {
	templateUrl: 'tableList/table-list.template.html',
	controllerAs : 'tList',
	controller: function myTableListController($scope, $http, $routeParams) {
		var tList = this;
		tList.currentId = $routeParams.listId;
		tList.sort = false;
		tList.showAll = false;
		$http({
		method : 'POST',
		url : 'http://localhost:5000/api/get-custom-list'
		}).then(function(res){
			if(res.data[0] != null){
				tList.items = res.data;
			}
		})
		.catch(function (err){
			tList.items = new Array();
		} )
	}
});