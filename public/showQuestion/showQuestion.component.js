angular.module('myApp').component('showQuestion', {
	templateUrl : 'showQuestion/show-question.template.html',
	controllerAs : 'myQue',
	controller : function myTableListController($scope, $http, $routeParams,$location, $cookies) {

		var myQue = this;
		myQue.showGreeting = false;
		myQue.showLogin = true;
		/*myQue.gotoBottom = function() {
		      // set the location.hash to the id of
		      // the element you wish to scroll to.
		      $location.hash('bottom-bar');

		      // call $anchorScroll()
		      $anchorScroll();
		  };*/
		  if($cookies.get('username') === 'admin'){
			  	myQue.showLogin = false;
		  		myQue.showGreeting = true;
		  }
		  myQue.userValidate =  function(){
		  	console.log('clicked');
		  	var user = angular.element( document.querySelector('#em')).val().toLowerCase();
		  	var password = angular.element( document.querySelector('#pas')).val().toLowerCase();
		  	if(user == 'admin' && password == 'admin'){
		  		$('.question_error_valid').animate({'opacity':'0'});
                $('.question_error_valid').css({'display':'none'});
                $cookies.put('username', user);
		  		myQue.showLogin = false;
		  		myQue.showGreeting = true;
		  	}else{
		  		$('.question_error_valid').animate({'opacity':'1'});
                $('.question_error_valid').css({'display':'block'});
		  	}
		  }
		  myQue.setBoolVal = function(key,val){
		  	console.log(key,val)
			myQue[key] = val;
		  }
		  myQue.age = [
			  {
				  label : "10-20",
				  text : 'years'
			  },
			  {
				  label : "21-25",
				  text : 'years'
			  },
			  {
				  label : "26-30",
				  text : 'years'
			  },
			  {
				  label : "31-40",
				  text : 'years'
			  },
			  {
				  label : "41-Above",
				  text : 'years'
			  }];
		  myQue.gender = ["Male","Female"];
		  myQue.frame = ["Small","Medium","Large"];
		  myQue.dairy = [
		  		{
		  			label : "1-2",
		  			text : 'Servings'
		  		},
		  		{
		  			label : "2-3",
		  			text : 'Servings'
		  		},
		  		{
					label : "3-4",
					text : 'Servings'

		  		}
		];
		  myQue.meat = [
		  		{
		  			label : "1-2",
		  			text : 'Servings'
		  		},
		  		{
		  			label : "2-3",
		  			text : 'Servings'
		  		},
		  		{
					label : "3-4",
					text : 'Servings'
		  		},
		  		{
		  			label : "4+",
					text : 'Servings'
		  		}
  		];
		  myQue.wheat = [
		  			{
			  			label : "1-6",
			  			text : 'Servings'
			  		},
			  		{
			  			label : "6-11",
			  			text : 'Servings'
			  		},
			  		{
						label : "11-15",
						text : 'Servings'
			  		},
			  		{
			  			label : "15+",
						text : 'Servings'
			  		}
		  ];
		  myQue.vegetables = [
				  	{
			  			label : "1-2",
			  			text : 'Servings'
			  		},
			  		{
			  			label : "2-3",
			  			text : 'Servings'
			  		},
			  		{
						label : "3-4",
						text : 'Servings'
			  		},
			  		{
			  			label : "4+",
						text : 'Servings'
			  		}
		  ];
		  myQue.fruits = [
		  			{
			  			label : "1-2",
			  			text : 'Servings'
			  		},
			  		{
			  			label : "2-3",
			  			text : 'Servings'
			  		},
			  		{
						label : "3-4",
						text : 'Servings'
			  		},
			  		{
			  			label : "4+",
						text : 'Servings'
			  		}];
		  myQue.water = [
		  			{
			  			label : "1-3",
			  			text : 'Glasses'
			  		},
			  		{
			  			label : "3-5",
			  			text : 'Glasses'
			  		},
			  		{
						label : "5-7",
						text : 'Glasses'
			  		},
			  		{
			  			label : "7+",
						text : 'Glasses'
			  		}
		  ];
		  myQue.diseases = ['Cholelethiasis', 'Gout', 'Osteoporosis', 'Obesity', 'Over Weight', 'Chronic Renal Failure', 'Chronic Obstructive Pulmonary Disorder', 'Arthritis', 'Diabetes', 'Heart Failure'];
		  myQue.sleep = [{
			  label: "0-5",
			  text: 'Hours'
		  }, {
			  label: "5-6",
			  text: 'Hours'
		  }, {
			  label: "7-8",
			  text: 'Hours'
		  }, {
			  label: "8+",
			  text: 'Hours'
		  }];
		  myQue.exercise_intensity = [ 'Light', 'Moderate', 'Heavy'];
		  myQue.exercise = [{
			  label: "0-80",
			  text: 'Minutes'
		  },{
			  label: "80-100",
			  text: 'Minutes'
		  }, {
			  label: "100-150",
			  text: 'Minutes'
		  }, {
			  label: "150+",
			  text: 'Minutes'
		  }];
		  myQue.dairyShow = '';
		  myQue.meatShow = '';
		  myQue.wheatShow = '';
		  myQue.vegetablesShow = '';
		  myQue.fruitsShow = '';
		  myQue.waterShow = '';
		  myQue.exerciseShow = '';
		  myQue.diseasesShow = '';
		  myQue.medicinesShow = '';
		  myQue.diseasesHistoryShow = '';
		  /*myQue.basicInfo = {
		  	page1: {attribute: 'name', value: ''}
		  }*/
		  myQue.basicInfo = {
		  	name:'',
		  	email:'',
		  	age:'',
		  	weight:'',
		  	height_f:'',
		  	height_i:'',
		  	gender:'',
		  	frame:'',
		  	// dairy:'',
		  	// meat:'',
		  	// wheat:'',
		  	// vegetables:'',
		  	// fruits:'',
		  	// water:'',
		  	// diseases:'',
		  	// medicines:'',
		  	sleep:'',
		  	exercise:'',
		  	exercise_intensity : '',
		  	smoke:'',
		  	// diseasesHistory : ''
		  };
  		$scope.animateMe = function($event) {
			angular.element($event.currentTarget)
				.fadeOut(175)
				.fadeIn(175);
		};

  myQue.saveAll = function () {
  	console.log(myQue.basicInfo);
		myQue.basicInfo.medicines = myQue.basicInfo.medicinesMention || myQue.basicInfo.medicines;
		myQue.basicInfo.diseasesHistory = myQue.basicInfo.diseasesHistoryMention || myQue.basicInfo.diseasesHistory;
  	var sendObj = myQue.basicInfo;
  	console.log('saveall called');
  	console.log(sendObj);
  	/*sendObj.answers = {};
			//var saveObj = [];
			for (var i = 0; i < myQue.len; i++) {
				sendObj.answers[myQue.data[i]._id] = myQue.data[i].answer
			}*/
			//console.log(myQue.data,sendObj);
			$http({
				method : 'POST',
				url : '/api/saveAns',
				data:sendObj
			}).then(function (res) {
				$location.path("/survey/show-results/" + res.data._id);
			})
			.catch (function (err) {})

		}

		$http({
			method : 'POST',
			url : '/api/show'
		}).then(function (res) {

			myQue.data = res.data;
			myQue.len = myQue.data.length;
			for (var i = 0; i < myQue.len; i++) {
				myQue.data[i].values = myQue.data[i].values.split(",");
				myQue.data[i].answer = '';
			}
		})
		.catch (function (err) {})

	}
});
