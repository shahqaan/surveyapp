angular.module('myApp').component('showResults', {
	templateUrl : 'showResults/show-results.template.html',
	controllerAs : 'myAns',
	controller: function($scope, $http, $routeParams, $location){
		var appID = 101646533626411;
		$scope.pageUrl = 'http://192.241.160.147:5000/#' + $location.path();

		// $scope.facebookShare = 'https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent($scope.pageUrl) +
		// '&picture=';
		$scope.showWaterImgs = false;
		$scope.showDairyImgs = false;
		$scope.showWheatImgs = false;
		$scope.triggerGauge = false;

		$scope.shareFB = function(){
			$http({
				method : 'POST',
				url : '/api/share/fb'
			})
			.then(function(res){
				console.log(res);
			})
			.catch(function(err){
				console.log(err);
			})
		};

		$http({
			method : 'GET',
			url : '/api/getMyResult/'+$routeParams.id
		}).then(function (res) {

			$scope.data = res.data;
			$scope.emailShare = 'mailto:' + $scope.data[0].email + '?subject=My%20Score&body=' + encodeURIComponent($scope.pageUrl);
			$scope.weight = parseInt($scope.data[0].weight);
			$scope.height_f = parseInt($scope.data[0].height_f);
			$scope.height_i = parseInt($scope.data[0].height_i);
			// Kgs/(((Feets*12)+Inches)*0.0254)^2
			$scope.bmi = $scope.weight/( Math.pow((($scope.height_f * 12) + $scope.height_i) * 0.0254, 2));
			// $scope.bmi = $scope.weight/(((-(-($scope.height_f*12)-$scope.height_i))*0.0254)*((-(-($scope.height_f*12)-$scope.height_i))*0.0254))
			$scope.bmi = parseFloat($scope.bmi).toFixed(1);
			var myBmiEl = angular.element( document.querySelector('#bmi'));
			var classBMI = '';
			var bmiMsg = '';
			var bmiScore = 0;
			if($scope.bmi < 17){
				classBMI = 'progress-bmi-1 ';
			}else if($scope.bmi >= 19 && $scope.bmi <= 24){
				classBMI = 'progress-bmi-2 ';
			}else if($scope.bmi >= 25 && $scope.bmi <= 29){
				classBMI = 'progress-bmi-3 ';
			}else if($scope.bmi >= 30 ){
				classBMI = 'progress-bmi-4 ';
			}
			if($scope.bmi >= 0 && $scope.bmi < 19) {
				bmiMsg = "Your BMI is at a Dangerous level! Your weight is below the mark than it actually should be according to your height. Please consult your nutritionist for better understanding.";
				bmiScore = 0;
			}
			else if( $scope.bmi >= 19 && $scope.bmi < 25 ) {
				bmiMsg = "Well Done with your BMI! Please continue to maintain your weight to enjoy a happy and healthy life.";
				bmiScore = 25 ;
			}
			else if ( $scope.bmi >= 25 && $scope.bmi < 30 ) {
				bmiMsg = "Watch out for your BMI! Your weight is above normal. You need to control your food intake and start following a healthy diet and workout plan. Balance is the key.";
				bmiScore = 12.5;
			}
			else {
				bmiMsg = "BMI Alert! This weight is an alarming situation for you. Obesity is a key contributor in the development of various diseases such as Diabetes, Cardiovascular diseases, and osteoporosis. Please consult your nutritionist for better understanding.";
				bmiScore = 0;
			}
			myBmiEl.addClass(classBMI);
			$scope.showProducts = {
				'dairy':false,
				'water':false,
				'wheat':false
			};
			var valueMap = {
				'gender':{
					'Male':{
						'progress': 'man-image',
					},
					'Female':{
						'progress': 'woman-image'
					}
				},
				'smoke':{
					'yes': {
						'text': 'Smoking is injurious to health and is a major risk factor for various diseases such as lung and throat cancer.',
						'progress': 'progress-smoke-1',
						'score':0
					},
					'no': {
						'text': 'Clean lungs helps you appreciate that breath of fresh air.',
						'progress': 'progress-smoke-2',
						'score':8.33
					}
				},
				'diseases':{
					"Cholelethiasis" : {
						progress : "progress-diseases-1",
						text : "You need to cut down on your daily fat intake. Consult your physician for further advice.",
						score : 0
					},
					"Gout" : {
						progress : 'progress-diseases-1',
						text : "Limit the high protein intake in your daily diet.",
						score : 0
					},
					"Osteoporosis" : {
						progress : 'progress-diseases-3',
						text : "Get your blood vitamin d levels checked and follow the bone health preserving and maintaining plan designed by your doctor.",
						score : 12.5
					},
					"Obesity" : {
						progress : 'progress-diseases-3',
						text : "Please consult your nutritionist for an effective weightloss and workout plan.",
						score : 12.5
					},
					"Over Weight" : {
						progress : 'progress-diseases-3',
						text : "You need to follow a strategic diet and exercise plan in order to reduce your weight. Please consult your nutritionist for further help and guidance.",
						score : 12.5
					},
					"Chronic Renal Failure" : {
						progress : 'progress-diseases-1',
						text : "Visit your Doctor  EVERY 6-12 MONTHS and follow the prescribed regime",
						score : 0
					},
					"Chronic Obstructive Pulmonary Disorder" : {
						progress : 'progress-diseases-1',
						text : "Get yourself examined every 6-12 months and act according to your physician's advice and prescription.",
						score : 0
					},
					"Arthritis" : {
						progress : 'progress-diseases-1',
						text : "Acting according to your physician's advice is your first and foremost priority. You need to get yourself regularly checked and avoid indulging in weight-bearing exercises.",
						score : 0
					},
					"Diabetes" : {
						progress : 'progress-diseases-1',
						text : "Check your blood glucose level on regular basis, follow a healthy diet and work out plan regularly. Visit your doctor after every three months.",
						score : 0
					},
					"Heart Failure" : {
						progress : 'progress-diseases-1',
						text : "Please visit your doctor on regular basis and follow all the prescribed work-plan given to you to manage your disease.",
						score : 0
					},
					'no': {
						'text': 'You are in perfect health',
						'progress': 'progress-diseases-2',
						'score':25
					}
				},
				'meat':{
					'no': {
						'text': 'Your intake of proteins is not adequate. Proteins help in muscle build up and tissue repair. They also provide essential nutrients such as iron, zinc and vitamin b12. 2-3 servings is recommended. Use items such meats, poultry, Maggi noodles or Neslte corn flakes everyday!',
						'progress':'progress-meat-1',
						'score':0,
						'img_url' : [{url : 'img/pic_1.jpg', text : 'Maggi'}]
					},
					'1-2':{
						'text': 'Your intake of proteins is not adequate. Proteins help in muscle build up and tissue repair. They also provide essential nutrients such as iron, zinc and vitamin b12. 2-3 servings is recommended. Use items such meats, poultry, Maggi noodles or Neslte corn flakes everyday!',
						'progress':'progress-meat-2',
						'score':0,
						'img_url' : [{url : 'img/pic_1.jpg', text : 'Maggi'}]
					},
					'2-3':{
						'text': 'That\'s what we are talking about. Eat right and stay healthy.',
						'progress':'progress-meat-3',
						'score':4.16
					},
					'3-4':{
						'text': 'Your intake of proteins is not adequate. Proteins help in muscle build up and tissue repair. They also provide essential nutrients such as iron, zinc and vitamin b12. 2-3 servings is recommended.',
						'progress':'progress-meat-4',
						'score':0
					},
					'4+':{
						'text': 'Your intake of proteins is not adequate. Proteins help in muscle build up and tissue repair. They also provide essential nutrients such as iron, zinc and vitamin b12. 2-3 servings is recommended.',
						'progress':'progress-meat-4',
						'score':0
					}
				},
				'dairy':{
					'no': {
						'text': 'Your intake of milk and milk products is not adequate. In order to live happy and strong, you need to have healthy bones and structure. Milk and milk products such as Nido Fortigrow, nestle Milkpak, Nesvita, nestle sweet n tasty, Nstle Ratas, Nstle Nesvita yogurt, nestle Acticol milk contain important bone building nutrients such as calcium, phosphorus & magnesium. Have at least 2-3 servings from this food group everyday!',
						'progress':'progress-dairy-1',
						'score':0,
						'img_url' : [{url : 'img/pic_3.jpg', text : 'NESTLE NIDO FORTIFIED'}, {url : 'img/pic_4.jpg', text : 'Nestle Milk Range'}, { url : 'img/pic_5.jpg', text : 'Nestle Yogurts'}]
					},
					'1-2':{
						'text': 'Your Intake Of Milk And Milk Products Is Not Adequate. In Order To Live Happy And Strong, You Need To Have Healthy Bones And Structure. Milk And Milk Products Such As Nido Fortigrow, Nestle Milkpak, Nesvita, Nestle Sweet N Tasty, Nestle Riatas, Nestle Nesvita Yogurt, Nestle Acticol Milk  Contain Important Bone Building Nutrients Such As Calcium, Phosphorus & Magnesium. Have At Least 2-3 Servings From This Food Group Everyday!',
						'progress':'progress-dairy-2',
						'score':0,
						'img_url' : [{url: 'img/pic_4.jpg', text: 'Nestle Milk Range'}, {url: 'img/pic_5.jpg', text: 'Nestle Yogurts'}]
					},
					'2-3':{
						'text': 'Well done! Continue to maintain eating healthy and adding a variety of foods in your daily diet. Enjoy healthy & happy living!',
						'progress':'progress-dairy-3',
						'score':4.16
					},
					'3-4':{
						'text': 'Your intake of milk and milk products is not adequate. In order to live happy and strong, you need to have healthy bones and structure. Milk and milk products contain important bone building nutrients such as calcium, phosphorus & magnesium. They also contain important proteins that are essential for a healthy lifestyle. Have at least 2-3 servings from this food group everyday!',
						'progress':'progress-dairy-4',
						'score':0
					}
				},
				'fruits':{
					'no': {
						'text': 'Your daily fruit intake is not adequate. Fruits contain fiber, vitamins, minerals and most importanly antioxidants which have immunity ehancing properties. Incorporate 2-3 servings of fruits in your daily diet!',
						'progress':'progress-fruits-1',
						'score':0
					},
					'1-2':{
						'text': 'Your daily fruit intake is not adequate. Fruits contain fiber, vitamins, minerals and most importanly antioxidants which have immunity ehancing properties. Incorporate 2-3 servings of fruits in your daily diet!',
						'progress':'progress-fruits-1',
						'score':0
					},
					'2-3':{
						'text': 'Good Job, Keep it up',
						'progress':'progress-fruits-2',
						'score':4.16
					},
					'3-4':{
						'text': 'Your daily fruit intake is not adequate. Fruits contain fiber, vitamins, minerals and most importanly antioxidants which have immunity ehancing properties. Incorporate 2-3 servings of fruits in your daily diet!',
						'progress':'progress-fruits-3',
						'score':0
					},
					'4+':{
						'text': 'Your daily fruit intake is not adequate. Fruits contain fiber, vitamins, minerals and most importanly antioxidants which have immunity ehancing properties. Incorporate 2-3 servings of fruits in your daily diet!',
						'progress':'progress-fruits-4',
						'score':0
					}
				},
				'wheat':{
					'no': {
						'text': 'Your Intake Of Wheat And Cereal Product Is Inadequate. This food group givesThe Quickest Energy Among All The Food Groups. Also Contain Important Nutrients Such As Vitamin B Complex & Fiber. Incorporate Products Like Maggi Noodles And Nestle Corn Flakes Which Are Ready Sources Of Carbohydrates.',
						'progress':'progress-wheat-1',
						'score':0,
						'img_url' : [{url : 'img/pic_1.jpg', text : 'Maggi'}, {url : 'img/pic_2.jpg', text : 'Nestle Breakfast Cereals'}]
					},
					'1-6':{
						'text': 'Your Intake Of Wheat And Cereal Product Is Inadequate. This food group givesThe Quickest Energy Among All The Food Groups. Also Contain Important Nutrients Such As Vitamin B Complex & Fiber. Incorporate Products Like Maggi Noodles And Nestle Corn Flakes Which Are Ready Sources Of Carbohydrates.',
						'progress':'progress-wheat-1',
						'score':0,
						'img_url' : [{url : 'img/pic_1.jpg', text : 'Maggi'}, {url : 'img/pic_2.jpg', text : 'Nestle Breakfast Cereals'}]
					},
					'6-11':{
						'text': 'Your Intake Of Wheat And Cereal Product Is Inadequate. This food group givesThe Quickest Energy Among All The Food Groups. Also Contain Important Nutrients Such As Vitamin B Complex & Fiber. Incorporate Products Like Maggi Noodles And Nestle Corn Flakes Which Are Ready Sources Of Carbohydrates.',
						'progress':'progress-wheat-2',
						'score':0,
						'img_url' : [{url : 'img/pic_2.jpg', text : 'Nestle Breakfast Cereals'}]
					},
					'11-15':{
						'text': 'That is just the right amount. Keep it up.',
						'progress':'progress-wheat-3',
						'score':4.16,
					},
					'15+':{
						'text': 'Your intake of wheat and cereal product is inadequate. Grains and cereal provide the maximum and the quickest energy among all the food groups. Not only do these food products provide you with energy but also contain important nutrients such as Vitamin B complex & fiber.',
						'progress':'progress-wheat-4',
						'score':0
					}
				},
				'sleep':{
					'0-5':{
						'text': 'You are not getting enough of sleep every night. Not enough sleep makes one weary and tired, may affect performance and activities of daily life. You should have at least 5 to 8 hours of sleep every night. ',
						'progress':'progress-sleep-1',
						'score':0
					},
					'5-6':{
						'text': 'You are not getting enough of sleep every night. Not getting enough sleep makes one weary and tired, may affect performance and activities of daily life. You should have at least 6 to 8 hours of sleep every night.',
						'progress':'progress-sleep-2',
						'score':4.16
					},
					'7-8':{
						'text': 'You are ready for the day',
						'progress':'progress-sleep-3',
						'score':8.33
					},
					'8+':{
						'text': 'Get up and face the world. Too much sleep with make you more lazy and lead to weight gain and other unwanted side effects. ',
						'progress':'progress-sleep-4',
						'score':0
					}
				},
				'vegetables':{
					'no': {
						'text': 'Your daily vegetable intake is not up to the mark. Vegetables are an important source of fiber for a healthy digestive system and supply vitamins and minerals for good health. It is recommended to incorporate a variety of vegetables in your daily diet!',
						'progress':'progress-veges-1',
						'score':0
					},
					'1-2':{
						'text': 'Your daily vegetable intake is not up to the mark. Vegetables are an important source of fiber for a healthy digestive system and supply vitamins and minerals for good health. It is recommended to incorporate a variety of vegetables in your daily diet!',
						'progress':'progress-veges-1',
						'score':0
					},
					'2-3':{
						'text': 'There is no such thing as too much vegetables as longs as they are green ones. So go green and stay lean. ',
						'progress':'progress-veges-2',
						'score':4.16
					},
					'3-4':{
						'text': 'Right amount of vegetables are an important source of fiber for a healthy digestive system and supply vitamins and minerals for good health.',
						'progress':'progress-veges-3',
						'score':0
					},
					'4+':{
						'text': 'Right amount of vegetables are an important source of fiber for a healthy digestive system and supply vitamins and minerals for good health.',
						'progress':'progress-veges-4',
						'score':0
					}
				},
				'water':{
					'no': {
						'text': 'Hydrating Your Body Is Essential As It Is Good For Skin, Regulates Body Temperature, Eliminates Wates And Enhances Your Performance. As A Rule Of Thumb, You Should Consume At Least 8 Glasses Of Safe And Healthy Water Everyday! ',
						'progress':'progress-water-1',
						'score':0
					},
					'1-3':{
						'text': 'Hydrating Your Body Is Essential As It Is Good For Skin, Regulates Body Temperature, Eliminates Wates And Enhances Your Performance. As A Rule Of Thumb, You Should Consume At Least 8 Glasses Of Safe And Healthy Water Everyday!',
						'progress':'progress-water-1',
						'score':0,
						'img_url' : [{ url : 'img/pic_6.jpg', text : 'NESTLE PURELIFE'}]
					},
					'3-5':{
						'text': 'Hydrating Your Body Is Essential As It Is Good For Skin, Regulates Body Temperature, Eliminates Wates And Enhances Your Performance. As A Rule Of Thumb, You Should Consume At Least 8 Glasses Of Safe And Healthy Water Everyday! ',
						'progress':'progress-water-2',
						'score':0,
						'img_url' : [{ url : 'img/pic_6.jpg', text : 'NESTLE PURELIFE'}]
					},
					'5-7':{
						'text': 'Hydrating Your Body Is Essential As It Is Good For Skin, Regulates Body Temperature, Eliminates Wates And Enhances Your Performance. As A Rule Of Thumb, You Should Consume At Least 8 Glasses Of Safe And Healthy Water Everyday! ',
						'progress':'progress-water-3',
						'score':0,
						'img_url' : [{ url : 'img/pic_6.jpg', text : 'NESTLE PURELIFE'}]
					},
					'7+':{
						'text': 'You are doing great. The recommended is at least 8 glasses of water a day and your body is thanking you. So maintain your hydration needs with Neslte Purelife and stay Happy, Healthy and full of life. ',
						'progress':'progress-water-4',
						'score':4.16
					}
				},
				'exercise':{
					'0-80':{
						'text': 'You need to increase your physical activity level in order to have strength and an active lifestyle. Working out helps increase bone mass and has overall health benefits.',
						'progress':'progress-exercise-1',
						'score':0
					},
					'80-100':{
						'text': 'You need to increase your physical activity level in order to have strength and an active lifestyle. Working out helps increase bone mass and has overall health benefits.',
						'progress':'progress-exercise-2',
						'score':4.16
					},
					'100-150':{
						'text': 'You are in the right track the secret to a healthy lifestyle is challenging your self and push forward.',
						'progress':'progress-exercise-3',
						'score':8.33
					},
					'150+':{
						'text': 'You are in the right track the secret to a healthy lifestyle is challenging your self and push forward. ',
						'progress':'progress-exercise-4',
						'score':8.33
					}
				}
			};
			$scope.totalScore = 0;
			$scope.imgToShow = [];
			for (var i = 0; i < $scope.data.length; i++){
			    var obj = $scope.data[i];
			    for (var key in obj){
			        var attrName = key;
			        var attrValue = obj[key];
			        if(typeof valueMap[key] !='undefined' && typeof valueMap[key][obj[key]] != 'undefined'){
				        var myEl = angular.element( document.querySelector('#'+key));
						var myEl2 = angular.element( document.querySelector('#'+key+'-re'));
						myEl.addClass(valueMap[key][obj[key]].progress);
						myEl2.addClass(valueMap[key][obj[key]].progress);
						var myEl1 = angular.element( document.querySelector('#'+key+"-text"));
						myEl1.html(valueMap[key][obj[key]].text);
						if(typeof valueMap[key][obj[key]].score != 'undefined'){
							var imgs;
							if(key == 'dairy' && valueMap[key][obj[key]].score != 4){
								$scope.showDairyImgs = Object.prototype.hasOwnProperty.call(valueMap[key][obj[key]], 'img_url');
								$scope.dairyImgs = valueMap[key][obj[key]].img_url;
							}
							if(key == 'water' && valueMap[key][obj[key]].score != 4){
								$scope.showWaterImgs = Object.prototype.hasOwnProperty.call(valueMap[key][obj[key]], 'img_url');
								$scope.waterImgs = valueMap[key][obj[key]].img_url;
							}
							if(key == 'wheat' && valueMap[key][obj[key]].score != 4){
								$scope.showWheatImgs = Object.prototype.hasOwnProperty.call(valueMap[key][obj[key]], 'img_url');
								$scope.wheatImgs = valueMap[key][obj[key]].img_url;
							}
							$scope.totalScore = $scope.totalScore + valueMap[key][obj[key]].score;
							console.log("Total : ", $scope.totalScore);
						}
					}
			    }
			}
			$scope.warning = false;
			$scope.danger = false;
			$scope.success = false;
			$scope.showExpert = ($scope.data[0].exercise === '0-80' || $scope.data[0].exercise === '80-100');
			$scope.totalScore = parseInt($scope.totalScore + bmiScore);
			if($scope.totalScore >= 0 && $scope.totalScore < 30){
				$scope.danger = true;
			}else if($scope.totalScore >= 30 && $scope.totalScore < 70){
				$scope.warning = true;
			}else{
				$scope.success = true;
			}
			$scope.scoreText = 'Sometimes things might not be under your control but then there are things you can take care of such as your eating habits, your sleep cycles and incorporating a healthyroutine.' + bmiMsg;
			if($scope.totalScore < 25){
				$scope.scoreText = 'You need to consider some changes in your attitude, behavior, lifestyle and your intake. Please visit your medical health professional for advise. ' + bmiMsg;
			}else if($scope.totalScore >= 25 && $scope.totalScore < 50){
				$scope.scoreText = 'Sometimes things might not be under your control but then there are things you can take care of such as your eating habits, your sleep cycles and incorporating a healthy routine. ' + bmiMsg;
			}else if($scope.totalScore >= 50 && $scope.totalScore < 75){
				$scope.scoreText = 'You are trying to make an effort for what is important to you and you need to continue working on your health.' + bmiMsg;
			}else if($scope.totalScore >= 75 ){
				$scope.scoreText = 'You are a role model to most of us. Keep it us and educate others. ';
			}

			$scope.triggerGauge = true;
			$scope.facebookShare = "http://www.facebook.com/dialog/feed?app_id=" + encodeURIComponent(appID) +
				'&link=' + encodeURIComponent($scope.pageUrl) +
				'&name=' + encodeURIComponent('Your Score is : ' + $scope.totalScore) +
				'&description=' + encodeURIComponent('Please Click here to see full result') +
				'&redirect_uri=' + encodeURIComponent($scope.pageUrl) +
				'&display=popup';
			$scope.twitterShare = 'https://twitter.com/share?url=' + encodeURIComponent($scope.pageUrl) +
				'&text=' + encodeURIComponent('Your Score is : ' + $scope.totalScore + "\nPlease click this link for details.\n");
		})
		.catch (function (err) {})
		}
});
