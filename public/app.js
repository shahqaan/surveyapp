'use strict';
var currentPageIndex = 0;
var numberOfPages = 19;
var bottomBarHeight = 80;
var pageHeight = window.innerHeight - bottomBarHeight;

function pageScroll(ele, position) {
	var translation = "translate3d(0,"+(-(position))+"px,0)";

    if (currentPageIndex == 0) {
        $('.bottom-bar').css('display', 'none');
    } else {
        $('.bottom-bar').css('display', 'block');
    }

	$('#main-page').css({
		'transform': translation,
		'-webkit-transform': translation,
		'-moz-transform': translation,
		'-o-transform': translation,
		'-ms-transform': translation,
        'transition': '0.5s'
	});
}

function setBarTitle(currentPageIndex){
	$(".progressed").css("width",currentPageIndex*5.6+"%");
	$(".bottom-bar p").html(currentPageIndex+` out of ${numberOfPages} Questions`);

	if(currentPageIndex < 8){
		$(".bottom-bar h5").html("Tell Us About Yourself");
	}

	if(currentPageIndex >= 8) {
		$(".bottom-bar h5").html("Eating Patterns");
	}

	if(currentPageIndex >= 14){
		$(".bottom-bar h5").html("Medical History");
	}

	if(currentPageIndex >= 16){
		$(".bottom-bar h5").html("Other Life Style");
	}
}

var myApp = angular.module('myApp',
	['ngRoute', 'angularValidator', 'ngCookies']);

myApp.config(['$routeProvider',
	function config($routeProvider) {

		$routeProvider
		.when('/survey', {
			template: '<show-question></show-question>'
		})
		.when('/survey/add-question', {
			template : "<add-question></add-question>"
		})
		.when('/survey/show-results/:id', {
			template : "<show-results></show-show-results>"
		})
		.otherwise('/survey')

	}
	])
	.directive('getClass', function() {
		return {
			link:function($scope, $element, $attributes){

					var key = $($element).attr('id');
					console.log($scope,$scope.data)
					if( $scope.data[0][key] == 'no'){
						$($element).addClass('progress-100');
					}


			}
		}
	})
	.directive('setGauge', function() {
		return {
			link:function($scope, $element, $attributes){
				$('body').css('overflow-y','scroll');
					$('.suggestions').css('width',(170*$('.suggestions .item').length));
					var opts = {
					  lines: 12, // The number of lines to draw
					  angle: 0.15, // The length of each line
					  lineWidth: 0.44, // The line thickness
					  pointer: {
					    length: 0.9, // The radius of the inner circle
					    strokeWidth: 0.035, // The rotation offset
					    color: '#000000' // Fill color
					  },
					  limitMax: 'false',   // If true, the pointer will not go past the end of the gauge
					  colorStart: '#e67e22',   // Colors
					  colorStop: '#e67e22',    // just experiment with them
					  strokeColor: '#E0E0E0',   // to see which ones work best for you
					  generateGradient: true
					};
					$scope.$watch('triggerGauge',function(){
						if(typeof $scope.totalScore != 'undefined'){
							var target = document.getElementById('my-gauge'); // your canvas element
							//#41d041 green
							//#f65d49 red
							//#ffa201 yellow
							if($scope.totalScore >= 0 && $scope.totalScore < 30){
								opts.colorStart = '#f65d49';
								opts.colorStop = '#f65d49';
							}else if($scope.totalScore >= 30 && $scope.totalScore < 70){
								opts.colorStart = '#f6ed36';
								opts.colorStop = '#f6ed36';
							}else{
								opts.colorStart = '#41d041';
								opts.colorStop = '#41d041';
							}
							var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
							gauge.maxValue = 100; // set max gauge value
							gauge.minValue = 0; // set max gauge value
							gauge.animationSpeed = 10; // set animation speed (32 is default value)
							gauge.set($scope.totalScore); // set actual value
						}
					});
					/*window.setTimeout(function(){
						var target = document.getElementById('my-gauge'); // your canvas element
						var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
						gauge.maxValue = 100; // set max gauge value
						gauge.animationSpeed = 10; // set animation speed (32 is default value)
						gauge.set($scope.totalScore); // set actual value


					},1000)*/


			}
		}
	})
	.directive('toggleActiveForBool', function($timeout) {
		return {
			link:function($scope, $element, $attributes){
				$element.click(function(){

					var valOfElement = $("#"+$element.attr("for")).attr("value");

					$timeout(function() {
						if(valOfElement === "no"){
							currentPageIndex++;
							pageScroll("body",currentPageIndex * pageHeight);
							var pageNow = currentPageIndex+1;
							setBarTitle(currentPageIndex);
						}
					}, 800);
				})

			}
		}
	})
	.directive('toggleActive', function($timeout) {
		return {
			link:function($scope, $element, $attributes){
				$element.click(function(){
                    $timeout(function() {
                        if(currentPageIndex !== numberOfPages) {
                            currentPageIndex++;
                            pageScroll("body",currentPageIndex * pageHeight);
                            var pageNow = currentPageIndex+1;
                            setBarTitle(currentPageIndex);
                        }
                    }, 800);
				})

			}
		}
	})
	.directive('setFullScreen', function() {
		return {
			link:function($scope, $element, $attributes){
				$element.css('height',window.innerHeight)
			}
		}
	})
	.directive('retakeSurvey', function() {
		return {
			link: function($scope, $elem) {
				$elem.on('click', function() {
					currentPageIndex = 0;
				})
			}
		}
	})
	.directive('stepScrolling', function() {
		return {
			link: function($scope, $element, $attributes) {
				$(".next-question").click(function(){ next(); });

                document.getElementById('main-page').addEventListener('touchmove', function(e) {
                    e.preventDefault();
                }, true);

				function prev(e){
					//console.log("currentPageIndex",currentPageIndex)
					if(!(currentPageIndex == 0)){
						currentPageIndex--;
						pageScroll("body",currentPageIndex * pageHeight);
						var pageNow = currentPageIndex+1;
						if(pageNow >1){
							//$(".bottom-bar").animate({"top":(pageNow * pageHeight)-100},825,"linear")
							setBarTitle(currentPageIndex);
						}
					}


				}

                function next(e) {
                	var regexTest = {
                		'name' : '([^\s]*)',
                		//'emailAddress':'^\\w+@[a-zA-Z_]',
                		'emailAddress': /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/,
                		'weight':'^[+-]?(\\d*\\.)?\\d+$',
                		'height_f':'^(2|3|4|5|6|7|8)$',
                		'height_i':'^(0|1|2|3|4|5|6|7|8|9|10|11)$'
                	};

                    if($scope.myQue.showGreeting){

	                    if($(".question_"+currentPageIndex).attr("type")=="radio"){
                            console.log('type radio');
	                        var name = $(".question_"+currentPageIndex).attr("name");
	                        var optional = $(".question_"+currentPageIndex).attr("optional") === undefined ? false : true;


	                        if(name.indexOf("_bool") > -1){
	                            var named = name.replace("_bool","");
	                            if($scope.myQue[named+"Show"]){
	                                if($scope.myQue.basicInfo[named] !=""){
	                                    ScrollNext();
	                                }
	                            }else if($scope.myQue[named+"Show"] === false){
	                                ScrollNext();
	                            }
	                        }else if($scope.myQue.basicInfo[name] !="" || optional){
	                        	if(optional){
	                            	var mname = name.replace("_bool","");
	                            	$scope.myQue.basicInfo[mname] = "No Answer";
	                        	}
	                            ScrollNext();
	                        }
	                    } else if($('.question_' + currentPageIndex).val() != '') {
	                        var remainingObjs = $(".question_"+currentPageIndex);
							$(".question_error_" + currentPageIndex).animate({'opacity':'0'});
							$(".question_error_" + currentPageIndex).css({'display':'none'});
                        	var optional = $('.question_' + currentPageIndex).attr('optional') === undefined ? false : true;

                            var isValid = true;

                            var isHeightFtFalse = false;
                            var isHeightInchesFalse = false;

                            $(".question_error_" + currentPageIndex + "_ft").animate({'opacity': '0'});
                            $(".question_error_" + currentPageIndex + "_inches").animate({'opacity': '0'});

	                        for(var i = 0; i<remainingObjs.length;i++){
								var regex = regexTest[$(remainingObjs[i]).attr('name')];
	                        	var getRegex = new RegExp(regex);
	                        	var value = $(remainingObjs[i]).val();

	                            if(value != "" && (getRegex.test(value) || value.match(regex)) ){
	                                optional = true;
	                            } else{
                                    if (currentPageIndex == 6) {
                                        if (i == 0) {
                                            isHeightFtFalse = true;
                                        }
                                        else {
                                            isHeightInchesFalse = true;
                                        }
                                    }
	                                isValid = false;
	                            }
	                        }

                            console.log('currentPageIndex: ', currentPageIndex, 'ft: ', isHeightFtFalse, 'inches: ', isHeightInchesFalse);

	                        if (isValid && optional) {
	                            ScrollNext();
	                            $(".question_error_" + currentPageIndex).animate({'opacity':'0'});
	                            $(".question_error_" + currentPageIndex).css({'display':'none'});
	                        } else {

                                if (currentPageIndex == 6) {
                                    if (isHeightFtFalse) {
                                        $(".question_error_" + currentPageIndex + "_ft").animate({'opacity': '1'});
                                        $(".question_error_" + currentPageIndex + "_ft").css({'display': 'block'});
                                    }

                                    if (isHeightInchesFalse) {
                                        $(".question_error_" + currentPageIndex + "_inches").animate({'opacity': '1'});
                                        $(".question_error_" + currentPageIndex + "_inches").css({'display': 'block'});
                                    }
                                } else {
                                    $(".question_error_" + currentPageIndex).animate({'opacity': '1'});
                                    $(".question_error_" + currentPageIndex).css({'display': 'block'});
                                }
	                        }
	                    }else{
	                    	$(".question_error_" + currentPageIndex).animate({'opacity':'1'});
	                    	$(".question_error_" + currentPageIndex).css({'display':'block'});
	                    }
                    if(currentPageIndex == 0){
                        ScrollNext();
                    }
                }

                }

                function ScrollNext(){
					if(currentPageIndex !== numberOfPages){
						if(!(currentPageIndex == (numberOfPages - 1))){
							currentPageIndex++;
							pageScroll("body",currentPageIndex * pageHeight);
							var pageNow = currentPageIndex+1;
							setBarTitle(currentPageIndex);
						}
					}
				}

				function recalibrate() {
					pageHeight = window.innerHeight - bottomBarHeight;
					$('.scroll-area').css('height', pageHeight);
					$('.full-screen').css('height', pageHeight);
				}

				recalibrate();

				$(".scroll-down").click(next);
				$(".scroll-up").click(prev);


				$("#main-page").scrollsteps({
					up: prev,
					down: next
				});

				$(window).resize(function() {
					recalibrate();
					pageScroll("body",currentPageIndex * pageHeight);
				});


			}
		}
	});
