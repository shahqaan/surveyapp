angular.module('myApp').component('addQuestion', {
	templateUrl : 'addQuestion/add-question.template.html',
	controllerAs : 'myQue',
	controller : function myTableListController($scope, $http, $routeParams) {
		var myQue = this;
		myQue.data = {
			question : "",
			type : "text",
			values : ['']
		}
		myQue.addOptions = function(){
			myQue.data.values.push('');
		}
		myQue.saveQuestion = function () {

			$http({
				method : 'POST',
				url : 'http://localhost:5000/api/add',
				data : myQue.data
			}).then(function (res) {
				console.log(res)
			})
			.catch (function (err) {
			})
		}
	}
});
