var Question = require('../models/question');
var Answer = require('../models/answer');

module.exports = {
	add : function(req, res){

		var newQuestion = new Question(req.body);
		newQuestion
			.save()
			.then(function(data){
				return res.status(200).send(data).send();
			})
		
	},
	get : function(req, res){
		Question.find({})
			.then(function(question){
				return res.status(200).send(question).end()
			})
	},
	saveAns: function(req, res){
		var newAnswer = new Answer(req.body)
		newAnswer
			.save()
			.then(function(data){
				return res.status(200).send(data).send();
			})
		console.log(newAnswer)
		
		
	},
	getAns : function(req, res){
		Answer.find({})
			.then(function(answer){
				return res.status(200).send(answer).end()
			})
	},
	getMyResult : function(req, res){
		Answer.find({"_id":req.params.id})
			.then(function(answer){
				return res.status(200).send(answer).end()
			})
	}
}