var express = require('express'),
	router = express.Router(),
	questionFunc = require('../controller/question.js');


router.post('/add', questionFunc.add)
	.post('/show', questionFunc.get)
	.post('/saveAns', questionFunc.saveAns)
	.post('/getAns', questionFunc.getAns)
	.get('/getMyResult/:id', questionFunc.getMyResult)

module.exports = router;