var express = require('express');
var router = express.Router();
var shareFunc = require('../controller/sharePost.js');

router
	.post('/share/fb', shareFunc.shareOnFB)
	.get('/fb/redirect', shareFunc.fbRedirect)



module.exports = router;