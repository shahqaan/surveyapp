'use strict';
//====================================================================================
//                            NameSpace - Section
//====================================================================================
const express = require('express'),
    app = express(),
    mongoose = require('mongoose'),
    bodyParser = require('body-parser');
//====================================================================================
//                            EnvironmentSetup - Section
//====================================================================================
var fs = require('fs');
// TODO : Uncomment this for adding the share post functionality. And Please refer to the controller/sharePost.js file to complete this
// 			functionality// var questionRouter = 
require('./routes/question');
var sharePost = require('./routes/sharePost');
var questionRouter = require('./routes/question');
var Answer = require('./models/answer');
var json2csv = require('json2csv');

global.appUrl = process.env.url || 'http://104.199.130.33:5000';
global.resultsPage = global.appUrl + '/#/survey/show-results/';


app.use(express.static('./public'));
app.use(bodyParser.json()); 

mongoose.connect('mongodb://localhost:27017/surveyDB');
var db =  mongoose.connection;

db.on('open', function(){
    console.log("Succefully Connected to database.");
})
.on('error',function(err){
    console.log("An Error Has Occuured. " + err);
});


app.use('/api', questionRouter);
// TODO : Uncomment this for adding the share post functionality. And Please refer to the controller/sharePost.js file to complete this
// 			functionality
// app.use('/api', sharePost);

app.get('/api/custom-list',function(req,res){
	res.end();
});

app.post('/api/custom-list',function(req,res){
	fs.writeFile('./public/listData/myList.json', JSON.stringify(req.body, null, 4), function (err) {
	  if (err) return console.log(err);
	});

	res.end();
});
app.post('/api/get-custom-list',function(req,res){
	
	fs.readFile('./public/listData/myList.json', 'utf8', function (err,data) {
	  if (err) {
		return console.log(err);
	  }
	  res.send(data);
	});
	
});

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({extended: false}));

app.get('/dashboard', function(req, res) {
    res.render('login', {});
});
app.post('/dashboard', function(req, res) {
    if (req.body.username && req.body.password &&
        req.body.username.toLowerCase() == 'admin' &&
        req.body.password == 'admin') {
        Answer.find({}).then(function (users) {
            res.render('index', {users: users});
        });
    } else {
        res.render('login', {});
    }
});

app.get('/user-answers.csv', function(req, res) {
	Answer.find({}).then(function(users) {
		var csv = json2csv({
			data: users,
			fields: [
				'name', 'email', 'age', 'weight', 'height_f', 'height_i', 'gender', 'frame', 'dairy', 'meat', 'wheat', 'vegetables', 'fruits', 'water', 'diseases',
                'diseasesHistory', 'medicines', 'sleep', 'exercise', 'smoke', 'exercise_intensity'
			]
		});
		res.send(csv);
	});
});

const port = 5000;
app.listen(port, function (err,result){
 if(!err){
   console.log("FeedFormulation App listening on port: ", port);
 }
});